<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8" />
		<title> Mon premier php </title>
	</head>
	<body>
		<!-- Methode 1 -->
		<h1>Hello world !</h1>

		<!-- Methode 2 -->
		<h1>
			<?php
			echo "Hello world !";
			?>
		</h1>

		<!-- Methode 3 -->
		<?php
		echo "<h1>Hello world !</h1>";
		?>

		<!-- Methode 4 -->
		<?php
		$texte = "<h1>Hello world !</h1>";
		echo $texte;
		?>
	</body>
</html>
