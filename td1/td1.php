<?php

	// Exercice 2

	$prenom = "<i>Dimitri</i>";
	$nom = "<i>Dennemont</i>";
	echo "<b>Nom</b> : $nom";
	echo " <b>Prénom</b> : $prenom";
	echo "<br/>";

	print_r(" <b>Nom</b> : $nom");
	print_r(" <b>Prénom</b> : $prenom");
	echo "<br/>";
	var_dump($nom);
	echo "<br/>";

	// Exercice 3 : Tableau indexé
	// Création d'un tableau
	$tableau = array('<li>Salade</li>','<li>Tomate</li>','<li>Oignon</li>');
	
	// Affichage du tableau
	foreach($tableau as $valeur){

		echo $valeur;
	}

	// Exercice 4 : Tableau associatif
	$menu = array(
		'1. Lundi' => 'Sauté mines',
		'2. Mardi' => 'Pain bouchons',
		'3. Mercredi' => 'Riz Cantonais',
		'4. Jeudi' => 'Boucané bringelles',
		'5. Vendredi' => 'Cari zourites',
	);

	foreach($menu as $cle=>$valeur){
		
		echo $cle. ' : ' .$valeur.'<br/>'; // Concaténation
	}
?>

	// Exercice 5
	<br>
	<a href="test_GET.php?taille=G">Je suis grand(e)</a>
	<br>
	<a href="test_GET.php?taille=P">Je suis petit(e)</a>
